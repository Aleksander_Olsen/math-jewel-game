#pragma once

#include "Globals.h"

class SDLClass
{
public:
	SDLClass();
	~SDLClass();
	SDL_Texture* LoadTexture(std::string path, SDL_Renderer* gRenderer);
	void FreeTexture(SDL_Texture* tex);
	void Render(int x, int y, SDL_Texture* sprite, SDL_Rect clip, SDL_Renderer* gRenderer);
};

