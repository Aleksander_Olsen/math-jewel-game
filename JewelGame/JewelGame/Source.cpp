//Main source file
//Author: Aleksander Olsen - 121233

#include <iostream>
#include "SDLClass.h"
#include "Board.h"


std::unique_ptr<Board> theBoard;

//Bool controlling when to stop the program.
auto running = true;

//SDL event handler.
SDL_Event event;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The SDL renderer.
SDL_Renderer* gRenderer = NULL;

//Starts up SDL and creates window
bool init();

//Frees media and shuts down SDL
void close();


bool init() {
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else {
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("Assignment 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else {

			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL) {
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else {
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags)) {
					printf("SDL_image could not initialize! SDL_mage Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

void close() {

	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Destroy renderer
	SDL_DestroyRenderer(gRenderer);
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}




int main(int argc, char* args[]) {

	//Start up SDL and create window
	if (!init()) {
		printf("Failed to initialize!\n");
	}
	else {

		theBoard = std::make_unique<Board>();

		while (running) {

			//Clear screen
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0x00);
			SDL_RenderClear(gRenderer);


			//Check for input.
			while (SDL_PollEvent(&event)) {
				if (event.type == SDL_QUIT) {
					running = false;
				}
				if (event.type == SDL_KEYDOWN) {
					switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						running = false;
						break;
					default:
						running = true;
					}
				}
				if (event.type == SDL_MOUSEBUTTONDOWN) {
					int mX;
					int mY;
					SDL_GetMouseState(&mX, &mY);
					theBoard->selectPiece(mX / (PIECE_SIZE + 2 * PIECE_OFFSET) + MAP_OFFSET, mY / (PIECE_SIZE + 2 * PIECE_OFFSET) + MAP_OFFSET);
				}
			}

			theBoard->render(gRenderer);

			//Update screen
			SDL_RenderPresent(gRenderer);


		}
	}

	//Free resources and close SDL
	close();

	return 0;
}