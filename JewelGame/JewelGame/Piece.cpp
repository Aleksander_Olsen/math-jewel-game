#include "Piece.h"



Piece::Piece()
{
}

Piece::Piece(int x, int y) {
	posX = x;
	posY = y;

	if ((posX > 1 && posX < (MAP_SIZE + MAP_OFFSET)) && (posY > 1 && posY < (MAP_SIZE + MAP_OFFSET))) {
		type = static_cast<PieceType>((rand() % (LAST - 2)) + 1);
	} else {
		type = BLANK;
	}
}


Piece::~Piece()
{
}

void Piece::render(SDL_Renderer * gRenderer) {
	
		//Render red filled quad
		shape = { (posX * (PIECE_SIZE + (2*PIECE_OFFSET)) + PIECE_OFFSET) - (MAP_OFFSET * (PIECE_SIZE + (2 * PIECE_OFFSET))),
				  (posY * (PIECE_SIZE + (2*PIECE_OFFSET)) + PIECE_OFFSET) - (MAP_OFFSET * (PIECE_SIZE + (2 * PIECE_OFFSET))),
				  PIECE_SIZE, PIECE_SIZE };
		switch (type)
		{
		case RED:
			SDL_SetRenderDrawColor(gRenderer, 0xFF, 0x00, 0x00, 0xFF);
			break;
		case GREEN:
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0xFF, 0x00, 0xFF);
			break;
		case BLUE:
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0xFF, 0xFF);
			break;
		case PURPLE:
			SDL_SetRenderDrawColor(gRenderer, 0x80, 0x00, 0x80, 0xFF);
			break;
		case YELLOW:
			SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0x00, 0xFF);
			break;
		case ORANGE:
			SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xA5, 0x00, 0xFF);
			break;
		default:
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
			break;
		}
		SDL_RenderFillRect(gRenderer, &shape);
	
}

void Piece::drawBorder(SDL_Renderer * gRenderer) {
	//Render green outlined quad
	SDL_Rect outlineRect = { (posX * (PIECE_SIZE + (2 * PIECE_OFFSET))) - (MAP_OFFSET * (PIECE_SIZE + (2 * PIECE_OFFSET))),
							 (posY * (PIECE_SIZE + (2 * PIECE_OFFSET))) - (MAP_OFFSET * (PIECE_SIZE + (2 * PIECE_OFFSET))),
							 PIECE_SIZE + 2*PIECE_OFFSET, PIECE_SIZE + 2*PIECE_OFFSET };
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderDrawRect(gRenderer, &outlineRect);
}

int Piece::getPosX()
{
	return posX;
}

int Piece::getPosY()
{
	return posY;
}

PieceType Piece::getType()
{
	return type;
}

void Piece::setType(PieceType _type) {
	type = _type;
}
