//Author: Aleksander Olsen - 121233

#pragma once

#include "Globals.h"

class Piece {
private:
	PieceType type;
	int posX;
	int posY;
	SDL_Rect shape;
public:
	Piece();
	Piece(int x, int y);
	~Piece();
	void render(SDL_Renderer* gRenderer);
	void drawBorder(SDL_Renderer* gRenderer);

	int getPosX();
	int getPosY();
	PieceType getType();
	void setType(PieceType _type);
};

