#include "Board.h"

Board::Board() {
	//Initialize board.
	for (int i = 0; i < (MAP_SIZE+ MAP_OFFSET*2); i++) {
		std::vector<std::shared_ptr<Piece>> column;
		for (int j = 0; j < (MAP_SIZE+ MAP_OFFSET*2); j++) {
			column.push_back(std::make_shared<Piece>(i, j));
		}
		map.push_back(std::move(column));
	}

	//The shapes (Theses will be moved to file)
	shapes.push_back({ -2,0, -1,0, 0,0, 1,0, 2,0 });
	shapes.push_back({ 0,-2, 0,-1, 0,0, 0,1, 0,2 });
	shapes.push_back({ 0,-1, 0,0, 0,1 });
	shapes.push_back({ -1,0, 0,0, 1,0 });

	//Check if any of the shapes mathecs the board, starting with the largest.
	for (auto s : shapes) {
		checkMatch(s);
	}

	selectedPiece = nullptr;
	switchPiece = nullptr;
}


Board::~Board()
{
}

void Board::checkMatch(std::vector<int> shape) {
	bool match;
	int newX;
	int newY;
	PieceType checkType;
	PieceType thisType;
	

	//Go through all the pieces to see if the pattern is found and removed.
	for (int i = MAP_OFFSET; i < (MAP_SIZE + MAP_OFFSET); i++) {
		for (int j = MAP_OFFSET; j < (MAP_SIZE + MAP_OFFSET); j++) {
			thisType = map.at(i).at(j)->getType();

			match = true;
			for (int k = 0; k < shape.size(); k++) {
				newX = i + shape.at(k);
				newY = j + shape.at(++k);
				checkType = map.at(newX).at(newY)->getType();
				if (checkType != thisType) {
					match = false;
					break;
				}
			}

			if (match) {
				removeMatch(i, j, shape);
			}
		}
	}
}

//When the mouse button is pressed, first check if a piece is already selected.
//If so, switch the already selected and the newly pressed pieces.
//If not, set the piece to selected.
void Board::selectPiece(int x, int y) {
	if (selectedPiece != nullptr) {
		switchPieces(x, y);
	} else {
		selectedPiece = map.at(x).at(y);
	}
}

void Board::switchPieces(int x, int y) {
	PieceType temp = selectedPiece->getType();
	selectedPiece->setType(map.at(x).at(y)->getType());
	map.at(x).at(y)->setType(temp);
	selectedPiece = nullptr;

	for (auto s : shapes) {
		checkMatch(s);
	}
}

//Removes the matching pattern from the board.
void Board::removeMatch(int posX, int posY, std::vector<int> match) {
	int newX;
	int newY;
	int numRuns;
	std::vector<std::shared_ptr<Piece>> toRemove;
	std::shared_ptr<Piece> nowRemoving;

	//Find all the pieces that belong to the pattern and add them to the array.
	for (int i = 0; i < match.size(); i++) {
		newX = posX + match.at(i);
		newY = posY + match.at(++i);
		toRemove.push_back(map.at(newX).at(newY));
	}

	//While there is still pieces left in the toRemove array.
	while (!toRemove.empty()) {
		//start with the last element of the array and remove it from the array.
		nowRemoving = toRemove.back();
		toRemove.pop_back();
		numRuns = 1;

		//Go through the rest of the array and find pieces with the same x coordinates.
		for (int i = 0; i < toRemove.size(); i++) {
			if (nowRemoving->getPosX() == toRemove.at(i)->getPosX()) {
				//For each piece it find with the same x coordinate, add 1 to the times it must remove on that x-axis.
				numRuns++;
				//If the y-coordinate of the new piece is higher (longer down), set this piece as the start point
				//so that it only runs on the piece furthest down in the pattern.
				if (toRemove.at(i)->getPosY() > nowRemoving->getPosY()) {
					nowRemoving = toRemove.at(i);
				}
				//Remove the piece from the array and step back.
				toRemove.erase(std::remove(toRemove.begin(), toRemove.end(), toRemove.at(i)), toRemove.end());
				i--;
			}
		}

		newX = nowRemoving->getPosX();
		newY = nowRemoving->getPosY();

		for (int i = 0; i < numRuns; i++) {
			//Start from the bottom of the pattern.
			nowRemoving = map.at(newX).at(newY);
			//Run for each piece till the top of the screen.
			while (nowRemoving->getPosY() > MAP_OFFSET) {
				//Set each piece to the type of the piece abowe it.
				nowRemoving->setType(map.at(nowRemoving->getPosX()).at(nowRemoving->getPosY()-1)->getType());
				nowRemoving = map.at(nowRemoving->getPosX()).at(nowRemoving->getPosY() - 1);
			}
			//The top most piece is set to a random type.
			nowRemoving->setType(static_cast<PieceType>((rand() % (LAST - 2)) + 1));
		}
	}

	//Check again if there has become any new patterns.
	for (auto s : shapes) {
		checkMatch(s);
	}

}

void Board::render(SDL_Renderer * gRenderer) {
	//Render each piece.
	for (auto &x : map) {
		for (auto &y : x) {
			y->render(gRenderer);
		}
	}

	if (selectedPiece != nullptr) {
		selectedPiece->drawBorder(gRenderer);
	}
}
