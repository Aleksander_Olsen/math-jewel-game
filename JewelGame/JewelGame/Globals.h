//Global variables and includes
//Author: Aleksander Olsen - 121233

#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <SDL.h>
#include <SDL_image.h>

enum PieceType {BLANK, RED, BLUE, GREEN, PURPLE, YELLOW, ORANGE, LAST};

//Screen dimension constants
const auto SCREEN_WIDTH = 800;
const auto SCREEN_HEIGHT = 800;

const auto MAP_SIZE = 16;
const auto MAP_OFFSET = 2;

const auto PIECE_SIZE = 46;
const auto PIECE_OFFSET = 2;