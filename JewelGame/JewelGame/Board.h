#pragma once

#include "Piece.h"
#include <memory>
#include <algorithm>

class Board {
private:
	std::vector<std::vector<std::shared_ptr<Piece>> > map;
	std::shared_ptr<Piece> selectedPiece;
	std::shared_ptr<Piece> switchPiece;
	std::vector<std::vector<int>> shapes;
public:
	Board();
	~Board();
	void checkMatch(std::vector<int> shape);
	void selectPiece(int x, int y);
	void switchPieces(int x, int y);
	void removeMatch(int x, int y, std::vector<int> match);
	void render(SDL_Renderer* gRenderer);
};

