
HOW TO MAKE PROJECT WORK IN VISUAL STUDIO 2015

1. Unzip SDL2.zip.
2. Put the folder SDL2 in the first folder named "Ass1AI"

1. Unzip SDL_DLLs.zip.
2. Put dll files in imt3591-a.i.-assignment-1\Ass1AI\Debug after first build.

Add include libraries:
- Go to project settings -> C/C++ -> general -> Additional Include Directories -> edit
- add "..\SDL2\SDL2-2.0.3vs15\include"
- add "..\SDL2\SDL2_image-2.0.0\include"

Add library:
- Go to project settings -> Linker -> Additional Library Directories -> edit
- add "..\SDL2\SDL2-2.0.3vs15\lib\x86"
- add "..\SDL2\SDL2_image-2.0.0\lib\x86"

- Go to Linker -> Input -> Additional Dependencies ->edit
- add SDL2.lib, SDL2main.lib, SDL2_image.lib

- Go to Linker -> System
- SubSystem sould be set to Console.




Author: Aleksander Olsen.